% =====================================
% Demo: ODE- vs. Agent-based simulation
% =====================================

% Setup main model parameters
theta = 0.4;
d = 4;
initialAdoptionsProbability = 0.05;

% ODE-specific parameters
startTime = 0;
stopTime = 150;

% Agent-based model parameters
numAgents = 75888;
numTrials = 100; % for the agent-based model

% -------------------------------------------------------
% Solve ODE to compute the fraction of adopters over time
% -------------------------------------------------------
[Time_ode, Adoptions_ode] = simODE (theta, d, ...
				    initialAdoptionsProbability, ...
				    startTime, stopTime);

% -------------------------------------------------------
% Simulate an agent-based model
% -------------------------------------------------------
numSteps = stopTime - startTime + 1;

%% Download
%Epinions1Data = UFget('SNAP/soc-Epinions1');

% Extract the sparse matrix representing who-trusts-whom
%Network = Epinions1Data.A;

% Note the number of agents in this network
%numAgents = min (size (Network));
%degreeE = nnz(Network) / max (size (Network)); %~6.7051 with Epinions1Data


% Optional: Visualize the sparse matrix (Network)
%spy (Network)

%Network = []; % Assume fully connected
Network = makeRandomNetwork (numAgents, 6.7051); % random


tic; % start timer
Adoptions_total = zeros (numSteps, numTrials);
for trial=1:numTrials,
  Adoptions = simAgents (numAgents, Network, theta, d, ...
			 initialAdoptionsProbability, numSteps);
  Adoptions_total(:, trial) = Adoptions;
end
fprintf ('  (Done; elapsed time: %g seconds)\n', toc);

Time_agents = (1:numSteps)';
Adoptions = Adoptions_total / numAgents; % normalize

% -------------------
% Compare the results
% -------------------
plotAdoptionsODEvsAgents (Time_ode, Adoptions_ode, ...
			  Time_agents, Adoptions_total / numAgents);
title (sprintf ('Fraction of adopters: %d agents [%d trial(s)]', numAgents, numTrials));

% eof
